import React, {useState, useEffect} from 'react';
import { Space, Table, Tag, Collapse , message, Layout, Select, Button} from 'antd';
import { Input } from 'antd';
import { useNavigate} from 'react-router-dom';

const { Search } = Input;
const { Panel } = Collapse;
const { Header, Footer, Sider, Content } = Layout;



const transaction_columns = [
  {
    title: 'Time',
    dataIndex: 'time',
    key: 'time',
    render: (text) => <a>{text}</a>,
  },
  {
    title: 'Coin',
    dataIndex: 'symbol',
    key: 'symbol',
    render: (text) => <a>{text}</a>,
  },
  {
    title: 'Hash',
    dataIndex: 'transactionHash',
    key: 'transactionHash',
  },
  {
    title: 'Amount',
    dataIndex: 'amount',
    key: 'amount',
  },
  {
    title: 'From',
    dataIndex: 'from',
    key: 'from',
  },
  {
    title: 'To',
    dataIndex: 'to',
    key: 'to',
  },
  
];

const blanace_columns = [
  {
    title: 'Time',
    dataIndex: 'time',
    key: 'time',
    render: (text) => <a>{text}</a>,
  },
  {
    title: 'Block number',
    dataIndex: 'number',
    key: 'number',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: 'Balance',
    dataIndex: 'balance',
    key: 'balance',
  },
]

const App = () => {
  const nav = useNavigate()
  const [transaction, setTransaction] = useState({
    address: "",
    contract: "",
    symbol: "",
    name: "",
    transactions: []
  });
  const [balance, setBalance] = useState({
    points: [],
    address:""
  });
  const [addresses, setAddresses] = useState([]);
  const [contracts, setContracts] = useState([]);

  const [messageApi, contextHolder] = message.useMessage();
  const error_message = ({error}) => {
    messageApi.open({
      type: 'error',
      content: error.code,
    });
  };
  const success_message = () => {
    messageApi.open({
      type: 'success',
      content: "Load success!",
    });
  };
  

  const getTransaction = async()=>{
    console.log("transaction", transaction)
    const token = localStorage.getItem("token");
    try{
        const response = await fetch(`${process.env.REACT_APP_API_URL}/addresses/transactions?address=${transaction.address}&contract=${transaction.contract}`,{
          'headers': {
            'Authorization': `Bearer ${token}`,
          }
        });
        const {error, data} = await response.json();
        console.log(error, data)
        if(error){
          if(error.code === "invalid_token"){
            localStorage.removeItem("token")
            nav("/login")
            return
          }
          error_message({error})
          return
        }
        success_message()
        setTransaction({
          ...transaction,
          name:  data.name,
          symbol: data.symbol,
          transactions: data.transactions.map((item)=>{
            return {
              key: item.transactionHash,
              amount: item.amount,
              name:  data.name,
              symbol: data.symbol,
              from: item.from,
              to: item.to,
              transactionHash: item.transactionHash,
              time:  new Date(item.timestamp).toISOString()
            }
          })
        });
    }catch(e){
        console.error(`ERROR`, e)
        error_message({error:e})
    }
  }
  const getBalance = async()=>{
    console.log("balance", balance)
    const token = localStorage.getItem("token");
    try{
        const response = await fetch(`${process.env.REACT_APP_API_URL}/addresses/balance?address=${balance.address}`,{
          'headers': {
            'Authorization': `Bearer ${token}`,
          }
        });
        const {error, data} = await response.json();
        console.log(error, data)
        if(error){
          if(error.code === "invalid_token"){
            localStorage.removeItem("token")
            nav("/login")
            return
          }
          error_message({error})
          return
        }
        success_message()
        setBalance({
          ...balance,
          points: data.points.map((item)=>{
            return {
              key: item.number,
              balance: item.balance,
              number: item.number,
              address: balance.address,
              time:  new Date(item.time).toISOString()
            }
          }).reverse()
        });
    }catch(e){
        console.error(`ERROR`, e)
        error_message({error:e})
    }
  }
  const getAddresses = async()=>{
    console.log("addresses", addresses)
    const token = localStorage.getItem("token");
    try{
        const response = await fetch(`${process.env.REACT_APP_API_URL}/addresses`,{
          'headers': {
            'Authorization': `Bearer ${token}`,
          }
        });
        const {error, data} = await response.json();
        console.log(error, data)
        if(error){
          if(error.code === "invalid_token"){
            localStorage.removeItem("token")
            nav("/login")
            return
          }
          error_message({error})
          return
        }
        setAddresses(data.addresses.reverse())
    }catch(e){
        console.error(`ERROR`, e)
        error_message({error:e})
    }
  }
  const getContracts = async()=>{
    console.log("contracts", contracts)
    const token = localStorage.getItem("token");
    try{
        const response = await fetch(`${process.env.REACT_APP_API_URL}/addresses/contracts`,{
          'headers': {
            'Authorization': `Bearer ${token}`,
          }
        });
        const {error, data} = await response.json();
        console.log(error, data)
        if(error){
          if(error.code === "invalid_token"){
            localStorage.removeItem("token")
            nav("/login")
            return
          }
          error_message({error})
          return
        }
        setContracts(data.contracts.reverse())
    }catch(e){
        console.error(`ERROR`, e)
        error_message({error:e})
    }
  }
 //useEffect
  useEffect(() => {
    const token = localStorage.getItem("token");
    if(!token){
        nav("/login")
        return
    }
    getAddresses()
    getContracts()
  },[balance, transaction]);
  return (
    <>
    {contextHolder}
    <Header>
      <Button type="text" danger size={"large"} style={{
          "float": "right"
        }}
        onClick={()=>{
          localStorage.removeItem("token")
          nav("/login")
        }}
        >
        Logout
      </Button>
    </Header>
    <Collapse accordion>
    <Panel header="Balance history" key="2">
        <Space >
          <Select
              style={{
                width: 120,
              }}
              allowClear
              options={addresses.map((item)=>{
                return {...item, label:item.value}
              })}
              onChange={(value)=>{
                setBalance({
                  ...balance,
                  address: value
                })
              }}
          />
          <Search placeholder="input address" value={balance.address} allowClear onSearch={getBalance} onChange={(e)=>{
            setBalance({...balance, address: e.target.value})
          }} enterButton  />
         
        </Space>
        <Table columns={blanace_columns} dataSource={balance.points} />
      </Panel>
      <Panel header="Transaction" key="1">
        <Space >
          <Select
                style={{
                  width: 120,
                }}
                allowClear
                options={contracts.map((item)=>{
                  return {...item, label:item.value}
                })}
                onChange={(value)=>{
                  setTransaction({
                    ...transaction,
                    contract: value
                  })
                }}
          />
          <Input placeholder="input contract" value={transaction.contract} allowClear onChange={(e)=>{
            setTransaction({...transaction, contract: e.target.value})
          }} />
          <Select
              style={{
                width: 120,
              }}
              allowClear
              options={addresses.map((item)=>{
                return {...item, label:item.value}
              })}
              onChange={(value)=>{
                setTransaction({
                  ...transaction,
                  address: value
                })
              }}

          />
          <Search placeholder="input address" value={transaction.address}  allowClear onSearch={getTransaction} enterButton onChange={(e)=>{
            setTransaction({...transaction, address: e.target.value})
          }}  />
        </Space>
        <Table columns={transaction_columns} dataSource={transaction.transactions} />
      </Panel>
    </Collapse>
    </>
  )
}
  
export default App;