import React,{useState, useEffect} from 'react';
import { Button, Form, Input,  Col, Row, message } from 'antd';
import { useNavigate} from 'react-router-dom';

const App = () => {
   
    const [state, setState] = useState({email:"", password:""});
    const nav = useNavigate()
    const onFinish = (values) => {
        console.log('Success:', values);
    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    const [messageApi, contextHolder] = message.useMessage();

    const error_message = ({error}) => {
        messageApi.open({
          type: 'error',
          content: error.code,
        });
      };
    const create_account = async()=>{
        nav("/signup")
    };
    const login = async()=>{
        console.log("state", state)
        try{
            const response = await fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: state.email,
                    password: state.password
                })
            });
            const {error, data} = await response.json();
            console.log(error, data)
            if(error){
                error_message({error})
                return
            }
            localStorage.setItem("token", data.token)
            nav('/')

        }catch(e){
            console.error(`ERROR`, e)
            error_message({error:e})
        }
    }
    //useEffect
    useEffect(() => {
        const token = localStorage.getItem("token");
        if(token){
            nav("/")
            return
        }
    });
    return (
        <>
        {contextHolder}
        <Row>
            <Col span={8}></Col>
            <Col span={8}>
                <Form
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
                
                >
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your email!',
                        }
                    ]}
                    
                >
                    <Input  onChange={(e)=>{
                        setState({...state, email: e.target.value})
                    }} />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                    {
                        required: true,
                        message: 'Please input your password!',
                    },
                    ]}
                >
                    <Input.Password   onChange={(e)=>{
                        setState({...state, password: e.target.value})
                    }}/>
                </Form.Item>

                <Form.Item
                    name="remember"
                    valuePropName="checked"
                    wrapperCol={{
                    offset: 8,
                    span: 16,
                    }}
                >
                    
                </Form.Item>

                <Form.Item
                    wrapperCol={{
                    offset: 8,
                    span: 16,
                    }}
                >
                    <Button type="primary" htmlType="submit" onClick={login}>
                        Login
                    </Button>
                    <Button type="primary"   htmlType="button"
                        style={{
                            margin: '0 8px',
                        }}  
                        onClick={create_account}
                    >
                        Create an Account
                    </Button>
                </Form.Item>
                </Form>
            </Col>
            <Col span={8}></Col>
        </Row>
        </>
    );
};
export default App;